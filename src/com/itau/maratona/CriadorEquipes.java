package com.itau.maratona;

import java.util.ArrayList;
import java.util.List;

public class CriadorEquipes {
		
	public List<Equipe> construirEquipe(List<Aluno> alunos){
		List<Equipe> equipes = new ArrayList<>();
		
		for(int i = 0; i < 3 ; i++) {
		    Equipe equipeAluno = new Equipe();
			
			equipeAluno.alunos[i] = (Aluno) alunos;
			
			equipes.add(equipeAluno);
			
		}
		return equipes;
	}
	
	

}

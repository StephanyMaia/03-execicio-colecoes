package com.itau.maratona;

import java.util.List;

public class App {
	public static void main(String[] args) {
		Arquivo arquivo = new Arquivo("src/alunos.csv");
		List<Aluno> alunos = arquivo.ler();
		CriadorEquipes novaEquipe = new CriadorEquipes();
		System.out.println(novaEquipe.construirEquipe(alunos));
		
		System.out.println(alunos);
//		List<Equipe> equipes = novaEquipe.construirEquipe();
	}

}
